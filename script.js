class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

	static random(xFrom, xTo, yFrom, yTo) {
		const x = +(Math.random() * (xTo - xFrom) + xFrom).toFixed(1);
		const y = +(Math.random() * (yTo - yFrom) + yFrom).toFixed(1);
		return new Point(x, y)
	}
}

class Rectangle {
	constructor(point1, point2) {
		this.point1 = point1;
		this.point2 = point2;
		this.b = new Line(point1, new Point(point1.x, point2.y)).lenght;
		this.a = new Line(point1, new Point(point2.x, point1.y)).lenght;
	}

	isSquare() {
		return this.a === this.b
	}

	get area() {
		return this.a * this.b
	}

	contains(point) {
		return point.x >= this.point1.x && point.x <= this.point2.x && point.y >= this.point1.y && point.y <= this.point2.y
	}

	intersect(rectangle) {
		if (this.contains(new Point(rectangle.point1.x, rectangle.point1.y)) || this.contains(new Point(rectangle.point2.x, rectangle.point2.y)) || this.contains(new Point(rectangle.point1.x, rectangle.point2.y)) || this.contains(new Point(rectangle.point2.x, rectangle.point1.y))) {
			const left = Math.max(this.point1.x, rectangle.point1.x);
			const top = Math.min(this.point2.y, rectangle.point2.y);
			const right = Math.min(this.point2.x, rectangle.point2.x);
			const bottom = Math.max(this.point1.y, rectangle.point1.y);

			return new Rectangle(new Point(left, bottom), new Point(right, top))
		}
	}
}

class Ellipse {
	constructor(rect) {
		this.point1 = rect.point1;
		this.point2 = rect.point2;
		this.b = rect.b;
		this.a = rect.a;
	}

	isCircle() {
		return this.a === this.b
	}

	contains(point) {
		const a2 = (this.point1.x + this.point2.x) / 2;
		const b2 = (this.point1.y + this.point2.y) / 2;

		return (point.x - a2) ** 2 / this.a ** 2 + (point.y - b2) ** 2 / this.b ** 2 <= 1
	}

	getPoints() {
		const arr = [];
		while (arr.length < 10) {
			const point = Point.random(this.point1.x, this.point2.x, this.point1.y, this.point2.y);

			if (this.contains(new Point(point.x, point.y))) {
				arr.push(point);
			}
		}
		return arr
	}

	get area() {
		return Math.PI * this.a * this.b
	}
}

class Line {
	constructor(point1, point2) {
		this.point1 = point1;
		this.point2 = point2;
	}

	get lenght() {
		return +(Math.sqrt((this.point2.x - this.point1.x) ** 2 + (this.point2.y - this.point1.y) ** 2)).toFixed(2)
	}
}

let rect1Point1 = new Point(2, 4);
let rect1Point2 = new Point(12, 10);
let rect2Point1 = new Point(4, 6);
let rect2Point2 = new Point(16, 12);

const rect1 = new Rectangle(rect1Point1, rect1Point2);
const rect2 = new Rectangle(rect2Point1, rect2Point2);
const intersectRect = rect1.intersect(rect2);

const ellipse = new Ellipse(intersectRect);
const points = ellipse.getPoints();

console.log('Прямокутник 1: ', rect1);
console.log('Прямокутник 2: ', rect2);
console.log('Прямокутник утворений перетином: ', intersectRect);

console.log(points);